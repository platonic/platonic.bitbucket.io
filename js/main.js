jQuery(document).ready(function($) {
  var startclick = 0,
    startswipe = 0;
  $(document).on('click', '.burder_menu', function() {
    $('nav').addClass('visible_nav');
    $('#popup_bcg').fadeIn('slow');
  });
  $("nav").on( "mousedown", function(event) {
    startclick = event.pageX;
  });
  $("nav").on( "mousemove", function(event) {
    if (startclick > (event.pageX + 50)) closenav();
  });
  $("nav").on( "touchstart", function(e) {
    posX = e.targetTouches[0].clientX;
    startswipe = posX;
  });
  $("nav").on( "touchmove", function(e) {
    posX = e.targetTouches[0].clientX;
    if (startswipe > (posX + 50)) closenav();
  });

  $('#popup_bcg').click(function() {
    closenav();
  });
  $(document).on('click', '.close', function() {
    closenav();
  });
  $(document).on('click', '.popup_clicker', function() {
    $('.popup').fadeIn('slow');
    $('#popup_bcg').fadeIn('slow');
    $('.popup').css('top', ((($('section').height() + $('header').height()) / 2) - (($('.popup').height() + 120) / 2)));
  });
  if ($('body .page-wrap').hasClass('full_season_page')) {
    $('.carousel').carousel();
  }

});

function closenav() {
  $('#popup_bcg').fadeOut('slow');
  $('nav').removeClass('visible_nav');
  $('.popup').fadeOut('slow');
}
